package homework.task_6;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MainTask6 {
    private static Logger log = LogManager.getLogger(MainTask6.class);
    public static void runTask6(){

        log.trace("-----------------------FOR-MYSELF------------------------------");
        Path path = Paths.get("/Users/alesejpismennyj/Developer/");
        log.trace("The Path Name is: " + path);
        for (int i = 0; i < path.getNameCount(); i++) {
            log.info(" Element " + i + " is: " + path.getName(i));
        }
        log.trace("-----------------------TASK------------------------------------");
        try {
            Path path2 = Paths.get("/Users/alesejpismennyj/Developer/");
            Files.list(path2)
                    .filter(p -> !Files.isDirectory(p)).map(p -> p.toAbsolutePath()).forEach(log::info);
        } catch (IOException e) {

        }
    }

}

