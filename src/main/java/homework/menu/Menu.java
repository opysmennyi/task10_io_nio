package homework.menu;

import homework.task_1.MainTask1;
import homework.task_2.MainTask2;
import homework.task_3.MainTask3;
import homework.task_5.MainTask5;
import homework.task_6.MainTask6;
import homework.task_7.MainTask7;
import homework.task_8.RunTask8;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {


    private Map<String, String> menu;
    private Map<String, Functional> methodsMenu;
    private static final Scanner input = new Scanner(System.in);
    private static final Logger log = LogManager.getLogger(Menu.class);

    private Menu() {
        initMenu();
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();



        menu.put("1", "insert 1  for Run task 1");
        menu.put("2", "insert 2  for Run task 2");
        menu.put("3", "insert 3  for Run task 3");
        menu.put("4", "insert 4  for Run task 4");
        menu.put("5", "insert 5  for Run task 5");
        menu.put("6", "insert 6  for Run task 6");
        menu.put("7", "insert 7  for Run task 7");
        menu.put("8", "insert 8  for Run task 8");

        menu.put("0", "insert 0 for Close menu");

        methodsMenu.put("1", MainTask1::runTask1);
        methodsMenu.put("2", MainTask2::runTask2);
        methodsMenu.put("3", MainTask3::runTask3);
        methodsMenu.put("5", MainTask5::runTask5);
        methodsMenu.put("6", MainTask6::runTask6);
        methodsMenu.put("7", MainTask7::runTask7);
        methodsMenu.put("8", RunTask8::runTask8);
        methodsMenu.put("0", this::exit);


    }
    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }
    public void exit() {
        log.trace("Go out of here I don't wanna see you any more");
        System.exit(0);
    }

    public static void openMenu() {
        String key;
        Menu menu = new Menu();
        while (true) {
            try {
                System.out.println("\n");
                menu.outputMenu();
                System.out.println("Lets make a decision");
                key = input.nextLine();
                System.out.println("\n\n");
                menu.methodsMenu.get(key).run();
            } catch (NullPointerException e) {
                log.warn("\nWhat do you do???? Of cause it was wrong input! ");
            } catch (IOException e) {
                log.error("\nOMG upload new file please this one crashed");
            } catch (ClassNotFoundException e) {
                log.error("\nThat is your mistake you've could create this class but you didn't");
            } catch (Exception e) {
                log.error("By-by that was an ERROR");
            }
        }
    }
}
