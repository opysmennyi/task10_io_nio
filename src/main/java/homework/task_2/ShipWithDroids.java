package homework.task_2;
import java.io.Serializable;

public class ShipWithDroids  implements Serializable{
    private static final long serialVersionUID = 1L;
    private transient String name;
    private final int age;
    private final String type;

    public ShipWithDroids(String name, int age, String type) {
        this.name = name;
        this.age = age;
        this.type = type;
    }
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "ShipWithDroids{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", type=" + type +
                '}';
    }
}
