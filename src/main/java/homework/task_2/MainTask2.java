package homework.task_2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MainTask2 {

    private static Logger log = LogManager.getLogger(MainTask2.class);

    private static List<ShipWithDroids> getShipWithDroids(File dataFile) throws IOException, ClassNotFoundException {
        List<ShipWithDroids> shipWithDroids1 = new ArrayList<ShipWithDroids>();
        try (ObjectInputStream inputStream = new ObjectInputStream(
                new BufferedInputStream(new FileInputStream(dataFile)))) {
            while (true) {
                Object object = inputStream.readObject();
                if (object instanceof ShipWithDroids)
                    shipWithDroids1.add((ShipWithDroids) object);
            }
        } catch (EOFException e) {
        }
        return shipWithDroids1;
    }


    private static void createShipWithDroids(List<ShipWithDroids> shipWithDroids1, File dataFile) throws IOException {
        try (ObjectOutputStream out = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(dataFile)))) {
            for (ShipWithDroids shipWithDroids : shipWithDroids1) out.writeObject(shipWithDroids);
        }
    }

    public static void runTask2() throws IOException, ClassNotFoundException {
        List<ShipWithDroids> shipWithDroids = new ArrayList<ShipWithDroids>();
        shipWithDroids.add(new ShipWithDroids("Raceta", 52, "Sky1"));
        shipWithDroids.add(new ShipWithDroids("Roketa", 16, "Sky2"));
        File dataFile = new File("shipWithDroids.data");
        createShipWithDroids(shipWithDroids, dataFile);
        log.info("This method returns an array with transient String name");
        log.trace(getShipWithDroids(dataFile));
    }

}

