package homework.task_5;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public  class MainTask5 {

    private static final String fileComments1 = "Task5.txt";
    private static Logger log = LogManager.getLogger(MainTask5.class);

    public static void main(String file) {
        List<String> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(file))) {
            list = stream
                    .map(s -> s.trim())
                    .filter(s -> s.startsWith("/"))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error!" + e);
        }
        list.forEach(System.out::println);
        log.info("Finded all comments in file " + file);
    }

    public static void runTask5() {
main(fileComments1);

    }
}

