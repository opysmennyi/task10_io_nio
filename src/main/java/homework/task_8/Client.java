package homework.task_8;

import homework.task_2.MainTask2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private static Logger log = LogManager.getLogger(MainTask2.class);

    public static void runClient() throws Exception {
        String[] string = new String[0];
        if (string.length != 1) {
            log.trace("Pass the server IP as the sole command line argument");
            return;
        }
        try (var socket = new Socket(string[0], 50863)) {
            log.info("Enter lines of text then Ctrl+D or Ctrl+C to quit");
            var scanner = new Scanner(System.in);
            var in = new Scanner(socket.getInputStream());
            var out = new PrintWriter(socket.getOutputStream(), true);
            while (scanner.hasNextLine()) {
                out.println(scanner.nextLine());
                log.trace(in.nextLine());
            }
        }
    }
}