package homework.task_8;

import homework.task_2.MainTask2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executors;

public class Server {
    // we need to input nc 127.0.0.1 50863 to console
    private static Logger log = LogManager.getLogger(MainTask2.class);


    public static void runServer() throws Exception {
        try (var listener = new ServerSocket(50863)) {
            log.info("The capitalization server is running...");
            var pool = Executors.newFixedThreadPool(20);
            while (true) {
                pool.execute(new Capitalizer(listener.accept()));
            }
        }
    }

    private static class Capitalizer implements Runnable {
        private Socket socket;

        Capitalizer(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            log.debug("Connected: " + socket);
            try {
                var in = new Scanner(socket.getInputStream());
                var out = new PrintWriter(socket.getOutputStream(), true);
                while (in.hasNextLine()) {
                    out.println(in.nextLine().toUpperCase());
                }
            } catch (Exception e) {
                log.error("Error:" + socket);
            } finally {
                try { socket.close(); } catch (IOException e) {}
                log.warn("Closed: " + socket);
            }
        }
    }
    }

