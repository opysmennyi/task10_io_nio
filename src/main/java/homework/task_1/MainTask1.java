package homework.task_1;

import homework.task_2.MainTask2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainTask1 {
    private static Logger log = LogManager.getLogger(MainTask2.class);
    public static void runTask1(){
        log.trace("Tricky task, its always in progress");
    }
}
