package homework.task_3;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;


public class MainTask3<fileLength2> {
    public MainTask3() throws IOException {
    }

    private static Logger log = LogManager.getLogger(MainTask3.class);

    public static void variant_1() throws IOException {
        RandomAccessFile file = new RandomAccessFile("/Users/alesejpismennyj/Developer/task10_IO_NIO/1.pdf", "rw");

        long fileLength = file.length() / 1024 / 1024;
        log.trace("Now we will hope and wait BECAUSE:");
        log.trace("file 1 length is: " + fileLength + " Mb");
        try {
            long timeAtBeggin = System.currentTimeMillis();

            InputStream regular =
                    new FileInputStream("/Users/alesejpismennyj/Developer/task10_IO_NIO/1.pdf");

            int data = regular.read();
            while (data != -1) {
                data = regular.read();
            }
            regular.close();

            long currentTime = System.currentTimeMillis();
            double timeInMinutes = (currentTime - timeAtBeggin) / 1000 / 60;


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        RandomAccessFile file1_1 = new RandomAccessFile("/Users/alesejpismennyj/Developer/task10_IO_NIO/1.pdf", "rw");

        long fileLength1_1 = file1_1.length() / 1024 / 1024;
        log.trace("file 1 length is: " + fileLength1_1 + " Mb");
        try {
            long timeAtBeggin1_1 = System.currentTimeMillis();

            InputStream regular1_1 = new DataInputStream(
                    new BufferedInputStream(
                            new FileInputStream("/Users/alesejpismennyj/Developer/task10_IO_NIO/1.pdf")));

            int data = regular1_1.read();
            while (data != -1) {
                data = regular1_1.read();
            }
            regular1_1.close();

            long currentTime1_1 = System.currentTimeMillis();
            double timeInMinutes1_1 = ((currentTime1_1 - timeAtBeggin1_1) / 1000 / 60);

            log.info("Read file with " + fileLength + " MB via buffer reader in seconds: " + (currentTime1_1 - timeAtBeggin1_1) / 1000);
            log.info("Read file with " + fileLength + " MB via buffer reader in minutes: " + timeInMinutes1_1);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //System.out.println("---------------------------------THECOND-FILE-------------------------------------------------------");
    public static void variant_2() throws IOException {
        RandomAccessFile file2_1 =
                new RandomAccessFile("/Users/alesejpismennyj/Developer/task10_IO_NIO/2.pdf", "rw");

        long fileLength2_1 = file2_1.length() / 1024 / 1024;
        log.trace("file 1 length is: " + fileLength2_1 + " Mb");
        try {
            long timeAtBeggin2_1 = System.currentTimeMillis();

            InputStream regular2_1 =
                    new FileInputStream("/Users/alesejpismennyj/Developer/task10_IO_NIO/2.pdf");

            int data2_1 = regular2_1.read();
            while (data2_1 != -1) {
                data2_1 = regular2_1.read();
            }
            regular2_1.close();

            long currentTime2_1 = System.currentTimeMillis();
            double timeInMinutes2_1 = ((currentTime2_1 - timeAtBeggin2_1) / 1000 / 60);

            log.info("Read file with " + fileLength2_1 + " MB without buffer reader in seconds: " + (currentTime2_1 - timeAtBeggin2_1) / 1000);
            log.info("Read file with " + fileLength2_1 + " MB without buffer reader in minutes: " + timeInMinutes2_1);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        RandomAccessFile file2_2 =
                new RandomAccessFile("/Users/alesejpismennyj/Developer/task10_IO_NIO/2.pdf", "rw");

        long fileLength2_2 = file2_2.length() / 1024 / 1024;
        log.trace("file 1 length is: " + fileLength2_2 + " Mb");
        try {
            long timeAtBeggin2_2 = System.currentTimeMillis();

            InputStream regular2_2 =
                    new DataInputStream(
                            new BufferedInputStream(
                                    new FileInputStream("/Users/alesejpismennyj/Developer/task10_IO_NIO/2.pdf")));

            int data2_2 = regular2_2.read();
            while (data2_2 != -1) {
                data2_2 = regular2_2.read();
            }
            regular2_2.close();

            long currentTime2_2 = System.currentTimeMillis();
            double timeInMinutes2_2 = (currentTime2_2 - timeAtBeggin2_2) / 1000 / 60;

            log.info("Read file with " + fileLength2_2 + " MB via buffer reader in seconds: " + (currentTime2_2 - timeAtBeggin2_2) / 1000);
            log.info("Read file with " + fileLength2_2 + " MB via buffer reader in minutes: " + timeInMinutes2_2);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void runTask3() throws IOException {
        variant_1();
        variant_2();

    }
}
